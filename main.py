import time, sys, os

def main():
    if not os.path.exists("./data"):
        os.makedirs("./data")
    while True:
        print(sys.argv[1])
        f = open("./data/test.txt", "a")
        f.write(sys.argv[1] + "\n")
        f.close()
        time.sleep(60)

if __name__ == "__main__":
    main()