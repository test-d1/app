FROM python:3.11-alpine
WORKDIR /app
COPY main.py .
VOLUME [ "/app/data" ]
CMD [ "python", "./main.py", "testArg1", "testArg2", "testArg3" ]